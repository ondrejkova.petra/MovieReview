class CreateWantToSees < ActiveRecord::Migration[5.0]
  def change
    create_table :want_to_sees do |t|
      t.date :date

      t.timestamps
    end
  end
end
