class AddUserIdToWantToSees < ActiveRecord::Migration[5.0]
  def change
    add_column :want_to_sees, :user_id, :integer
  end
end
