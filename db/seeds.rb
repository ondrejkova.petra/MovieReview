# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

Category.create(name: "Akční")
Category.create(name: "Romantický")
Category.create(name: "Historický")
Category.create(name: "Dokument")
Category.create(name: "Pohádka")
Category.create(name: "Horor")
Category.create(name: "Sci-fi")
Category.create(name: "Komedie")
Category.create(name: "Drama")
Category.create(name: "Životopisný")
Category.create(name: "Thriller")
Category.create(name: "Katastrofický")
Category.create(name: "Muzikál")
Category.create(name:"Fantasy")
