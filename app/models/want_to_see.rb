class WantToSee < ApplicationRecord
  belongs_to :user
  belongs_to :movie

  validate :date_validation
  validates :movie_id, uniqueness: { scope: :user_id, message: "You've wanted to see this movie to!" }

  def date_validation
    if date.nil?
      errors.add(:date, 'Date must be set')
    elsif date < Date.today
      errors.add(:date, 'Date can not be in past')
    end
  end

end
