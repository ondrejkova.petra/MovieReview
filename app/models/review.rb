class Review < ApplicationRecord
  belongs_to :user
  belongs_to :movie
  validates_presence_of :rating, :comment
  validates :movie_id, uniqueness: { scope: :user_id, message: "You've reviewed this movie!" }
end
