class Director < ApplicationRecord
  validate :date_validation
  has_many :movies

  has_attached_file :image, styles: { medium: "400x600#" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/

  validates_presence_of :name, :surname, :biography

  def date_validation
    if birth.nil?
      errors.add(:birth, 'Birth must be set')
    elsif birth >= Date.today
      errors.add(:birth, 'Birth can not be in future or today')
    end
  end

  def age
    today = Date.today
    age = today.year - birth.year
    age -= 1 if Date.today < (birth + age.years)
    age
  end

  def select_label
    "#{name} #{surname} " + birth.strftime("%d.%m.%Y")
  end
end
