class Movie < ApplicationRecord
  belongs_to :user
  has_many :reviews, dependent: :destroy
  has_many :want_to_sees, dependent: :destroy
  belongs_to :category
  belongs_to :director
  has_and_belongs_to_many :actors

  has_attached_file :image, styles: { medium: "400x600#" }
  validates_attachment_content_type :image, content_type: /\Aimage\/.*\z/
  validates_presence_of :title, :year, :lengthMinutes, :director, :description, :actors
  validate :length_validation
  validate :year_validation

  self.per_page = 1

  def avg_ratting
    @reviews = Review.where(movie_id: id)
    return 0 if @reviews.blank?
    @reviews.average(:rating).round(2)
  end

  def length_validation
    errors.add(:lengthMinutes, 'must be more then 0') if lengthMinutes && (lengthMinutes <= 0)
  end

  def year_validation
    errors.add(:year, 'must be more then 0') if year && (year <= 0)
  end
end
