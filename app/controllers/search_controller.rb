class SearchController < ApplicationController
  def search
    @persons = []
    @movies = []
    if params[:search].present?
      if params[:category].present?
        search_movies = Movie.where(
          "title LIKE ?",
          "%#{params[:search]}%") |
        Movie.where(
          "title LIKE ?",
          "%#{params[:search].capitalize}%")
        category_movie = Movie.where(category_id: params[:category])
        @movies = search_movies & category_movie
      else
        @movies = Movie.where(
          "title LIKE ?",
          "%#{params[:search]}%") |
        Movie.where(
          "title LIKE ?",
          "%#{params[:search].capitalize}%")
      end
      dir_name = Director.where(
        "surname LIKE ?",
        "%#{params[:search]}%") |
      Director.where(
        "surname LIKE ?",
        "%#{params[:search].capitalize}%")
      dir_surname = Director.where(
        "name LIKE ?",
        "%#{params[:search]}%") |
      Director.where(
        "name LIKE ?",
        "%#{params[:search].capitalize}%")
      act_name = Actor.where(
        "surname LIKE ?",
        "%#{params[:search]}%") |
      Actor.where(
        "surname LIKE ?",
        "%#{params[:search].capitalize}%")
      act_surname = Actor.where(
        "name LIKE ?",
        "%#{params[:search]}%") |
      Actor.where("name LIKE ?",
        "%#{params[:search].capitalize}%")
      if params[:work].present?
        if params[:work].to_i == 1
          @persons = act_name | act_surname
        else
          @persons = dir_name | dir_surname
        end
      else
        @persons = dir_name | dir_surname | act_name | act_surname
      end
    else
      if params[:category].present?
        @movies = Movie.where(category_id: params[:category])
      else
        @movies = []
      end
      if params[:work].present?
        if params[:work].to_i == 1
          @persons = Actor.all
        else
          @persons = Director.all
        end
      else
        @persons = []
      end
    end
    @movies = @movies.paginate(page: params[:movies], per_page: 4)
    @persons = @persons.paginate(page: params[:persons], per_page: 4)
  end
end
