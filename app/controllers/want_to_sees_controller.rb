class WantToSeesController < ApplicationController
  before_action :set_want_to_see, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!
  before_action :set_movie, only: [:new, :create, :update]

  # GET /want_to_sees
  # GET /want_to_sees.json
  def index
    @want_to_sees = []
      if params[:category].present?
        WantToSee.where(user_id: current_user.id).order(:date).each do |want_to_see|
          movie = Movie.where(id: want_to_see.movie_id, category_id: params[:category]).first
          @want_to_sees << movie if movie
        end
      else
        WantToSee.where(user_id: current_user.id).order(:date).each do |want_to_see|
          @want_to_sees << Movie.where(id:want_to_see.movie_id).first
        end
      end
      @want_to_sees = @want_to_sees.paginate(page: params[:want], per_page: 5)
  end

  # GET /want_to_sees/1
  # GET /want_to_sees/1.json
  def show
  end

  # GET /want_to_sees/new
  def new
    @want_to_see = WantToSee.new
  end

  # GET /want_to_sees/1/edit
  def edit
    @movie = Movie.where(id: @want_to_see.movie_id).first
  end

  # POST /want_to_sees
  # POST /want_to_sees.json
  def create
    @want_to_see = WantToSee.new(want_to_see_params)
    @want_to_see.user_id = current_user.id
    @want_to_see.movie_id = @movie.id
    respond_to do |format|
      if @want_to_see.save
        format.html { redirect_to want_to_sees_path, notice: 'New notice of seeing movie was succesfully created.' }
        format.json { render :index }
      else
        format.html { render :new }
        format.json { render json: @want_to_see.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /want_to_sees/1
  # PATCH/PUT /want_to_sees/1.json
  def update
    respond_to do |format|
      if @want_to_see.update(want_to_see_params)
        format.html { redirect_to want_to_sees_path, notice: 'Notice of seeing movie was succesfully updated.' }
        format.json { render :index, status: :ok, location: @want_to_see }
      else
        format.html { render :edit }
        format.json { render json: @want_to_see.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /want_to_sees/1
  # DELETE /want_to_sees/1.json
  def destroy
    @want_to_see.destroy
    respond_to do |format|
      format.html { redirect_to want_to_sees_url, notice: 'Notice of seeing movie was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private

    # Use callbacks to share common setup or constraints between actions.
    def set_want_to_see
      @want_to_see = WantToSee.find(params[:id])
    end

    def set_movie
      @movie = Movie.find(params[:movie_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def want_to_see_params
      params.require(:want_to_see).permit(:date)
    end
end
