Rails.application.routes.draw do

  resources :want_to_sees, except: [:show, :new, :create, :edit, :update]
  resources :actors, except: [:index]
  devise_for :users, :controllers => { :registrations => "users/registrations" }
  resources :directors, except: [:index]
  get "search" => "search#search", :as => :search

  resources :movies do
    resources :reviews, except: [:show, :index]
    resources :want_to_sees, only: [:new, :create, :edit, :update]
  end

  root 'movies#index'
end
