require 'test_helper'

class WantToSeesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @want_to_see = want_to_sees(:one)
  end

  test "should get index" do
    get want_to_sees_url
    assert_response :success
  end

  test "should get new" do
    get new_want_to_see_url
    assert_response :success
  end

  test "should create want_to_see" do
    assert_difference('WantToSee.count') do
      post want_to_sees_url, params: { want_to_see: { date: @want_to_see.date } }
    end

    assert_redirected_to want_to_see_url(WantToSee.last)
  end

  test "should show want_to_see" do
    get want_to_see_url(@want_to_see)
    assert_response :success
  end

  test "should get edit" do
    get edit_want_to_see_url(@want_to_see)
    assert_response :success
  end

  test "should update want_to_see" do
    patch want_to_see_url(@want_to_see), params: { want_to_see: { date: @want_to_see.date } }
    assert_redirected_to want_to_see_url(@want_to_see)
  end

  test "should destroy want_to_see" do
    assert_difference('WantToSee.count', -1) do
      delete want_to_see_url(@want_to_see)
    end

    assert_redirected_to want_to_sees_url
  end
end
